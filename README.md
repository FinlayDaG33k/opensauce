# OpenSauce
OpenSauce is an open-source repository for all kinds of delicious recipes in order to share the joy of good food from all over the world!  
You don't need to be a professional cook either, if you can make a delicious recipe, feel free you open up an MR :)  
Please try to keep ingredients to something that most people should have access too, or provide suitable substitutes.  
Additionally, please keep any measurement in the Metric system.  

Because it can be hard to find the original source of a recipe added by someone at times, I have decided to just trust people on giving the appropriate attributions.  
Recipes on this repository will not contain any licenses either, but if you decide to use or modify one, please give attribution to this repository.  

# Disclaimer:
All recipes are listed as is, under no circuimstance shall the repository maintainer or contributor of a recipe be held liable for any damages done.  
Please make sure you check the list of ingredients for yourself if you are allergic or intolerant to a specific allergen.  
If you find an unlisted allergen in a recipe that can't be attributed to a single specific brand, then feel free to open an MR to correct this.