# My awesome new recipe
<!-- Chef's notes (optional but appreciated), only the original contributor may edit this unless absolutely neccessary -->

# Quick Information
- People: <Number of people that could enjoy this>
- Time: <Estimated time it takes to make>
- Difficulty: <Low, Medium, High>
- Vegetarian: <Suitable for Vegetarians?>
- Vegan: <Suitable for Vegans?>

# Allergens
This recipe may cause issues for people that are allergic to:
<!-- Please add any allergen you are aware off here -->
- Unknown

<!-- Sorry, but have to put this here just in case -->
**DISCLAIMER**: This list might not be 100% accurate, please continue at own disgression.

# Ingredients
<!-- List any ingredients you need to use -->
| Ingredient | Amount | Comment |
|------------|--------|---------|


# Cooking the dish
<!-- Tell us the steps on how you should prepare the dish -->