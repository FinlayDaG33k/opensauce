# Advocaat
<!-- Chef's notes (optional but appreciated), only the original contributor may edit this unless absolutely neccessary -->
Recipe for a Dutch alcoholic "drink" that I've slightly modified.    
Not good for drinking yourself into a coma (no, that is not a challenge) but good for just a casual evening with friends.  
You can use a lot of different types of liquour.  
Traditionally, we use `brandy`, however, white rum is also very common.  
In case you didn't notice yet, this recipe contains alcohol, so if you're under the legal drinking age in your country, then please ask for permission from a parent or caretaker first.  
Drink responsibly!

# Quick Information
- People: 15
- Time: 15 minutes (excl. time for cooldown)
- Difficulty: Low
- Vegetarian: Yes
- Vegan: No

# Allergens
This recipe may cause issues for people that are allergic to:
<!-- Please add any allergen you are aware off here -->
- Eggs

<!-- Sorry, but have to put this here just in case -->
**DISCLAIMER**: This list might not be 100% accurate, please continue at own disgression.

# Ingredients
<!-- List any ingredients you need to use -->
| Ingredient | Amount | Comment |
|------------|--------|---------|
| Eggs | 5pcs | Use 8 egg yolks if not using the egg-whites | 
| Powdered Sugar | 250g | |
| Condensed Milk | 375ml | |
| Vanilla Sugar | 1pkt | You can add a second sachet to flavour |
| Liquour of Choice | 250ml | Add more "to taste" |

# Cooking the dish
<!-- Tell us the steps on how you should prepare the dish -->
1. Mix the eggs with the sugars and condensed milk in a bowl.
2. Add the liquour of choice and mix it until all sugar is dissolved.
3. Heat a pan with a small layer of water (about half a finger deep) until it's boiling (make sure the bowl fits in without touching the water)
4. Heat the mixture `au bain marie` while slowly keeping the mixture in movement (just enough to not have it sit idle and solidify)
5. Try to keep the core temperature of the mixture at a maximum of 62-63 degrees celsius (to prevent it from solidifying too much)
6. Once the temperature is reached, keep it steady and stir for about a minute.
7. Pour the still warm Advocaat in a sterile bottle of weckpot and store in the fridge for 24 hours (this will keep the Advocaat good for about 2 days); **or** put the bowl in cold water to cooldown if you want to consume it the same day.
8. Enjoy!