# Fried Noodles with Shrimp in Oystersauce
<!-- Chef's notes (optional but appreciated), only the original contributor may edit this -->
Recipe I came up with myself, inspired by a certain dish I always order at a certain restaurant.  
While you might think it's have a strong fish taste, in reality, I find that this isn't really true.  
It can actually be enjoyed even by people that might not normally like fish.  

# Quick Information
- People: 6 (or 5 with a good appetite)
- Time: 30 minutes
- Difficulty: Medium
- Vegetarian: Yes
- Vegan: No

# Allergens
This recipe may cause issues for people that are allergic to:
<!-- Please add any allergen you are aware off here -->
- Shellfish
- Gluten
- Eggs
- Soy

<!-- Sorry, but have to put this here just in case -->
**DISCLAIMER**: This list might not be 100% accurate, please continue at own disgression.

# Ingredients
<!-- List any ingredients you need to use -->
| Ingredient | Amount | Comment |
|------------|--------|---------|
| Eggnoodles | 600g | |
| Shrimps | 400g | de-shelled and de-veined |
| Onions | 2pcs | Decent sized, chopped into rings |
| Garlic | 3pcs | Finely minced |
| Carrots | 3pcs | Chopped in strips about 2mm thick and 3cm long |
| Beansprouts | 180g | |
| Spring Onions | 3pcs | Chopped into "pucks" of about 3mm thick |
| Sunflower Oil | 1 cup + 3 tbsp | |
| Soysauce | 1/4th cup | |
| Oystersauce | 1 cup | |

# Cooking the dish
<!-- Tell us the steps on how you should prepare the dish -->
1. Cook the noodles 1 or 2 minutes in hot (not boiling) water until soft then let them drain.
2. Heat the wok and heat the shrimp in them to get out most of the moisture.
3. Throw out the moisture
4. Add about 3 tbsp of oil sunflower oil and the garlic to the wok and stirfry until the garlic gets slightly golden brown.
5. Scoop the shrimp and garlic out of the oil using a skimmer and put aside.
6. Throw out any remaining moisture in the wok.
7. Add about 1 cup of sunflower oil to the wok and let it heat up.
8. Add the normal onions and fry until they became transparent.
9. Add the noodles, one cup oystersauce and 1/4th cup of soysauce to the heated oil and onions.
10. Mix the noodles with the oystersauce, sunflower oil and soysauce until it's evenly distributed (if the bottom of the wok becomes dry, add more sunflower oil to keep it from burning).
11. Add the beansprouts, carrots shrimp and spring onions then mix them in.
12. Keep mixing everything for a few minutes to give it some time to heat up.
13. Serve and eat!