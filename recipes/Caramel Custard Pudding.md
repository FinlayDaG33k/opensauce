# My awesome new recipe
Pudding recipe I found on [YouTube](https://www.youtube.com/watch?v=1_q8txKyg4E) that I decided to make and actually turned out really nicely.

# Quick Information
- People: 5
- Time: 30 minutes
- Difficulty: Low
- Vegetarian: Yes
- Vegan: No

# Allergens
This recipe may cause issues for people that are allergic to:
<!-- Please add any allergen you are aware off here -->
- Lactose
- Eggs

<!-- Sorry, but have to put this here just in case -->
**DISCLAIMER**: This list might not be 100% accurate, please continue at own disgression.

# Ingredients
<!-- List any ingredients you need to use -->
| Ingredient | Amount | Comment |
|------------|--------|---------|
| Sugar | 170g | |
| Water | 15ml | |
| Eggs | 6pcs | |
| Milk | 500ml | |
| Heavy cream | 200g | |
| Vanilla extract | 1tbsp|

# Cooking the dish
1. Add 80g of sugar and 15ml of water to a small pan
2. Put the pan on medium-low heat until the mixture starts to caramelize to form our caramel
3. Distribute the caramel among 5 pudding sized containers
4. Separate the egg yolks of 3 eggs from the whites in a bowl for mixing
5. Add to the egg yolks 3 whole eggs (egg whites and yolk)
6. Whisk the eggwhites and yolks until they appear to be one
7. Add 500ml of milk to a pan
8. Add the remaining 90 grams of sugar and 200g of heavy cream to the milk
9. Put the milk on a heater on medium-low heat and stir until the mixture is warm
10. Pour the mixter into the egg mixture while whisking it
11. Add 1tbsp of vanilla extract to it
12. Whisk in the vanilla extract
13. Run the mixture through a strainer while pouring it on top of the (now hardened) caramel inside our containers
14. Put the containers in a little bottom of hot water (about half way to the top), be careful not to spill water on the pudding
15. Bake the puddings (while still in the water) in an oven preheated at 150&deg;C for 50 minutes
16. Carefully remove the containers from the oven and the water and refrigerate for about 4 hours
17. Flip the pudding on a plate and let it fall out of the container
18. Enjoy!