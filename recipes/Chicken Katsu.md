# Chicken Katsu
<!-- Chef's notes (optional but appreciated), only the original contributor may edit this unless absolutely neccessary -->
Got this recipe from [Joshua Weissman's video](https://www.youtube.com/watch?v=D6diqH8RF4U).  
It's really tasty, and goes well with a lot of other asian foods.  
It can also be served on a sandwich, stored for (up to) a day to have a snack later, it's a really versatile food.

You can scale up the amount of chicken used without increasing the other ingredients to make the chicken itself.  
The other ingredients mainly are for the batter and for frying, so this is just a case: "just need enough to cover it all".  
This recipe does not include the Tonkatsu sauce or the cabbage.

# Quick Information
- People: 4 (implying 1 piece per person)
- Time: 30
- Difficulty: Low
- Vegetarian: No
- Vegan: No

# Allergens
This recipe may cause issues for people that are allergic to:
<!-- Please add any allergen you are aware off here -->
- Eggs

<!-- Sorry, but have to put this here just in case -->
**DISCLAIMER**: This list might not be 100% accurate, please continue at own disgression.

# Ingredients
<!-- List any ingredients you need to use -->
| Ingredient | Amount | Comment |
|------------|--------|---------|
| Chicken Breasts or thighs | 4 pcs | Could be substituted for pork |
| All purpose flour | 2 cups | |
| Eggs | 3 pcs | |
| Water | 30ml | |
| Panko | 210g | |
| Frying Oil | 2 cups | Preferably sunflower but can also be peanut oil, basically any oil used in asian cooking |
| Salt or miso | | Use to taste (beware, miso is pretty salty!) |

# Cooking the dish
<!-- Tell us the steps on how you should prepare the dish -->
1. Wrap the chicken in some plastic foil and flatten it until about 12mm thick.
2. Season the chicken and let it sit in the fridge overnight (can be done moments before, just doesn't give as much flavor).
3. Fill one bowl with all purpose flower, another one with the eggs and water and the last one with the panko crumbs.
4. Whisk the eggs and water together until it's nice and consistent.
5. Take a chicken breast, put it in the flower and cover it with flour.
6. Take out the chicken breast and gently shake it off to remove excess flour
7. Put the chicken breast into the egg mixture.
8. Using one hand (this will now be your "wet" hand, the other will be the "dry" hand), flip the egg and make sure everything gets coated in egg.
9. Using your wet hand, take out the chicken from the egg mixture and put it in the panko.
10. Using your dry hand, cover the chicken in panko and gently press to make it adhere better to the chicken breast
11. Using your dry hand, carefully lift out the chicken from the panko and move it onto a piece of parchement paper.
12. Repeat step 5 until you have done all chicken breasts, make sure to never touch the dry flour or panko with your wet hand. Additionally, put a piece of parchement paper between each piece of chicken (so they don't stick together)
13. Heat up the oil in a skillet or wok to medium or medium-high heat.
14. Gently drop a chicken breast into the oil.
15. Fry until crispy golden brown (about 3-5 minutes).
16. Flip the chicken over and fry until crispy golden brown on the bottom (also about 3-5 minutes)
17. Take out the chicken and put it on a rack to cooldown a bit (and lose some excess oil)
18. Repeat step 14 until all chicken has been fried.
19. Serve and enjoy!